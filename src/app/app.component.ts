import { Component } from '@angular/core';
import { User } from './interfaces/User';
import { UserService } from './service/user.service';
import { ResponseSomesDto } from './interfaces/ResponseSomesDto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'prueba-torres-torres';
  dataSource: User[] = [];

  constructor(private _userService: UserService) {
    this.getUsers()
  }

  

  getUsers(){
    this._userService.get().subscribe({
      next: (data: ResponseSomesDto<User>) => {
        this.dataSource = data.data
      }
    })
  }

}
