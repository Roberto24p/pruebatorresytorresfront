import { Component, Input } from '@angular/core';
import { User } from 'src/app/interfaces/User';
import { UserService } from 'src/app/service/user.service';
import { ResponseSomesDto } from 'src/app/interfaces/ResponseSomesDto';
import { UserDto } from 'src/app/interfaces/UserDto';
import { ResponseDto } from 'src/app/interfaces/ResponseDto';
import { OpenModalService } from 'src/app/service/open-modal.service';

@Component({
  selector: 'app-table-users',
  templateUrl: './table-users.component.html',
  styleUrls: ['./table-users.component.sass']
})
export class TableUsersComponent {
  displayedColumns: string[] = ['Usuario', 'Nombres', 'Apellidos', 'Departamento', 'Cargo', 'actions'];
  @Input() dataSource: User[] = [];

  constructor(private _userService: UserService, private dialog: OpenModalService){

  }
  delete(id: number){
    this._userService.delete(id).subscribe({
      next: (data: ResponseDto<void>) => {
        window.location.reload();
      }
    })
  }

  edit(user: UserDto){
    this.dialog.openModal(user)
  }




}
