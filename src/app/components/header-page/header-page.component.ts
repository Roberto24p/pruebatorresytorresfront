import { Component } from '@angular/core';
import { OpenModalService } from 'src/app/service/open-modal.service';
@Component({
  selector: 'app-header-page',
  templateUrl: './header-page.component.html',
  styleUrls: ['./header-page.component.sass'],
})
export class HeaderPageComponent {
  constructor(private dialog: OpenModalService) { }

  openFormUser():void{
    this.dialog.openModal(null);
  }
}
