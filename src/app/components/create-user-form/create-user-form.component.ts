import { Component, Input, Output, EventEmitter, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DepartamentDto } from 'src/app/interfaces/DepartamentDto';
import { PositionDto } from 'src/app/interfaces/PositionDto';
import { ResponseDto } from 'src/app/interfaces/ResponseDto';
import { ResponseSomesDto } from 'src/app/interfaces/ResponseSomesDto';
import { User } from 'src/app/interfaces/User';
import { UserDto } from 'src/app/interfaces/UserDto';
import { DepartamentService } from 'src/app/service/departament.service';
import { PositionService } from 'src/app/service/position.service';
import { UserService } from 'src/app/service/user.service';
@Component({
  selector: 'app-create-user-form',
  templateUrl: './create-user-form.component.html',
  styleUrls: ['./create-user-form.component.sass']
})
export class CreateUserFormComponent {
  departaments: DepartamentDto[] = []
  positions: PositionDto[] = []
  @Output() userCreateEvent = new EventEmitter<void>();
  @Input() user: UserDto = { departament_id: 0, position_id: 0 } as UserDto;
  constructor(private dialogRef: MatDialogRef<CreateUserFormComponent>,
    private dialog: MatDialog,
    private _userService: UserService,
    private _positionService: PositionService,
    private _departamentService: DepartamentService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public us: UserDto) {
    console.log(us)
    if (us != null) {
      this.user = us
    }
    _departamentService.get().subscribe({
      next: (data: ResponseSomesDto<DepartamentDto>) => {
        this.departaments = data.data
        console.log(this.departaments)
      }
    })
    _positionService.get().subscribe({
      next: (data: ResponseSomesDto<PositionDto>) => {
        this.positions = data.data
        console.log(this.positions)
      }
    })
  }


  createUser(): void {
    this._userService.create(this.user).subscribe({
      next: (data: ResponseDto<User>) => {
        if (data.success) {
          this.dialogRef.close();
          this.userCreateEvent.emit()
          window.location.reload();
        } else {
          let errors: string = ""
          for(const prop in data.errors){
            if(data.errors.hasOwnProperty(prop) ){
              errors = errors + "\n" + data.errors[prop]
            }
          }
          alert(errors)
        }
      }
    });
  }

  updateUser(): void {
    this._userService.update(this.user).subscribe({
      next: (data: ResponseDto<User>) => {
        if (data.success) {
          this.dialogRef.close();
          this.userCreateEvent.emit()
          window.location.reload();
        } else {
          let errors: string = ""
          for(const prop in data.errors){
            if(data.errors.hasOwnProperty(prop) ){
              errors = errors + "\n" + data.errors[prop]
            }
          }
          alert(errors)
        }

      }, error: (data: any) => {
        console.log(data)
      }
    });
  }

  closeModal(): void {
    this.dialogRef.close();
  }
  openModal(): void {
    this.dialog.open(CreateUserFormComponent, {
      width: '400px', // Ancho del modal
      height: '300px', // Altura del modal
    });
  }
}
