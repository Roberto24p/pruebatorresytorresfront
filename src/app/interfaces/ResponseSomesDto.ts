export interface ResponseSomesDto<T>{
    success: boolean,
    data: T[],
    message: string
}

