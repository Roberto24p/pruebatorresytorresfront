export interface UserDto{
    id: number,
    user:string,
    firstName: string,
    secondName: string,
    secondLastName: string,
    lastName: string,
    departament_id: number,
    position_id: number,
}