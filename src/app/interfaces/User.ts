import { DepartamentDto } from "./DepartamentDto"
import { PositionDto } from "./PositionDto"
export interface User{
    id: number
    user: string,
    firstName: string,
    secondName: string,
    lastName: string,
    secondLastName: string,
    departament_name: string,
    position_name: string
}