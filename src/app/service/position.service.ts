import { Injectable } from '@angular/core';
import { ResponseSomesDto } from '../interfaces/ResponseSomesDto';
import { Environment } from 'src/environment';
import { HttpClient } from "@angular/common/http"
import { Observable } from 'rxjs';
import { PositionDto } from '../interfaces/PositionDto';
@Injectable({
  providedIn: 'root'
})
export class PositionService {
  private urlAPi: string = Environment.apiUrl

  constructor(private httpClient: HttpClient) { }
  public get(): Observable<ResponseSomesDto<PositionDto>> {
    return this.httpClient.get<ResponseSomesDto<PositionDto>>(this.urlAPi+"position");
  }}
