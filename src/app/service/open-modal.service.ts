import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateUserFormComponent } from '../components/create-user-form/create-user-form.component';
@Injectable({
  providedIn: 'root'
})
export class OpenModalService {

  constructor(private dialog: MatDialog) { }
  @Injectable({
    providedIn: 'root'
  })
  openModal(user: any): void {
    this.dialog.open(CreateUserFormComponent, {
      width: '650px', // Ancho del modal
      height: '435px', // Altura del modal,
      data: user
    });
  }}
