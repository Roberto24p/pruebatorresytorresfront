import { UserDto } from './../interfaces/UserDto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http"
import { Observable } from 'rxjs';
import { User } from '../interfaces/User';
import { Environment } from 'src/environment';
import { ResponseSomesDto } from '../interfaces/ResponseSomesDto';
import { ResponseDto } from '../interfaces/ResponseDto';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private urlAPi: string = Environment.apiUrl
  constructor(private httpClient: HttpClient) { }
  dataSource:User[] = [];

  public get(): Observable<ResponseSomesDto<User>> {
    return this.httpClient.get<ResponseSomesDto<User>>(this.urlAPi+"user");
  }

  public create(user: UserDto ): Observable<ResponseDto<User>>{
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.httpClient.post<ResponseDto<User>>(this.urlAPi+"user",user,{headers});
  }

  public delete(id: number): Observable<ResponseDto<void>>{
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.httpClient.delete<ResponseDto<void>>(this.urlAPi+"user/"+id);
  }

  public update(user: UserDto): Observable<ResponseDto<User>>{
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.httpClient.put<ResponseDto<User>>(this.urlAPi+"user/"+user.id,user,{headers});
  }

}
