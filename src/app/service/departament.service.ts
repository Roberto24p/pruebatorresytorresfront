import { Injectable } from '@angular/core';
import { ResponseSomesDto } from '../interfaces/ResponseSomesDto';
import { Environment } from 'src/environment';
import { HttpClient } from "@angular/common/http"
import { Observable } from 'rxjs';
import { DepartamentDto } from '../interfaces/DepartamentDto';
@Injectable({
  providedIn: 'root'
})
export class DepartamentService {
  private urlAPi: string = Environment.apiUrl

  constructor(private httpClient: HttpClient) { }
  public get(): Observable<ResponseSomesDto<DepartamentDto>> {
    return this.httpClient.get<ResponseSomesDto<DepartamentDto>>(this.urlAPi+"departament");
  }
}
