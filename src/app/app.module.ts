import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TableUsersComponent } from './components/table-users/table-users.component';
import { MatTableModule } from '@angular/material/table';
import { HeaderPageComponent } from './components/header-page/header-page.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CreateUserFormComponent } from './components/create-user-form/create-user-form.component';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatIconModule} from '@angular/material/icon'; 
@NgModule({
  declarations: [
    AppComponent,
    TableUsersComponent,
    HeaderPageComponent,
    CreateUserFormComponent,
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    MatCardModule,
    HttpClientModule,
    MatDialogModule,
    FormsModule,
    MatSnackBarModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
